package com.example.pokerkit;


import com.example.pokerkit.Card.Suit;

public class Deck extends CardSet
{
	public Deck()
	{
		super();
		for (int i=0; i<13; ++i)
		{
			cards.add(new Card(i,Suit.DIAMOND));
			cards.add(new Card(i,Suit.SPADE));
			cards.add(new Card(i,Suit.CLUB));
			cards.add(new Card(i,Suit.HEART));
		}
	}
}
