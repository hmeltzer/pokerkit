package com.example.pokerkit;

public class Card 
{
	int num;
	enum Suit
	{
		CLUB,
		HEART,
		DIAMOND,
		SPADE
	};
	Suit suit;
	public Card(int num, Suit suit) {
		super();
		this.num = num;
		this.suit = suit;
	}
	public int getNum() {
		return num;
	}
	public Suit getShape() {
		return suit;
	}
}
