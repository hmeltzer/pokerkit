package com.example.pokerkit;

public class Poker {
	Player[] players = new Player[]{new Player(),new Player(),new Player(),new Player()};
	
	CardSet cardSet;
	
	int start = 0;
	
	public Poker(){	}

	public void round(){	
		cardSet = new Deck();
		
		for (int i = 0; i < players.length; i++) {
			players[(start+i)%players.length].hand = new Hand(cardSet.takeTopCard(),cardSet.takeTopCard()); 
		}
		
		start++;
	}
	
}
