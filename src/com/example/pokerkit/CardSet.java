package com.example.pokerkit;

import java.util.ArrayList;

public class CardSet {
	protected ArrayList<Card> cards;
	
	public CardSet()
	{
		cards = new ArrayList<Card>();
	}
	
	public void shuffle()
	{
		for (int i=0; i<cards.size()*cards.size(); ++i)
		{
			Card card = cards.remove((int)Math.random()*cards.size());
			cards.add((int)(Math.random()*cards.size()), card);
		}
	}
	
	public Card takeTopCard()
	{
		return cards.remove(cards.size()-1);
	}

}
